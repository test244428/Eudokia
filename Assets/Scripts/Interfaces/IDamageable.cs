﻿namespace Eudokia.Interfaces
{
    public interface IDamageable
    {
        public void ApplyDamage(int damage);
    }
}