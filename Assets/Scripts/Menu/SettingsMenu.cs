﻿using Eudokia.Model.Data;
using UnityEngine;

namespace Eudokia.Menu
{
    public class SettingsMenu : MonoBehaviour
    {
        [SerializeField] private AudioSettingsWidget _music;
        [SerializeField] private AudioSettingsWidget _sfx;

        private void Awake()
        {
            _music.SetModel(GameSettings.Instance.Music);
            _sfx.SetModel(GameSettings.Instance.Sfx);
        }

        public void ReturnToMainMenu()
        {
            gameObject.SetActive(false);
        }
    }
}