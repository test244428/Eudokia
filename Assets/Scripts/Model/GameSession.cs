﻿using Eudokia.GameLoop;
using Eudokia.Menu;
using Eudokia.Model.Data.Definitions.Difficulty;
using Eudokia.Model.Data.Definitions.Player;
using Eudokia.Utils;
using Eudokia.Utils.Pause;
using UnityEngine;

namespace Eudokia.Model
{
    public class GameSession : MonoBehaviour
    {
        [SerializeField] private GameLoopSystem _gameLoop;
        [SerializeField] private PlayableArea _playableArea;

        [Header("Difficult settings"), Space] [SerializeField]
        private DifficultyLevel _difficultyLevel;

        [Header("Player settings"), Space] [SerializeField]
        private CannonDefinition _cannonDef;

        [Header("Scene management"), Space] [SerializeField]
        private string _mainMenuScene;

        [SerializeField] private SceneService _sceneService;

        private StatModel _statModel;
        [SerializeField] private LevelData _playerLevels;
        public GameLoopSystem GameLoop => _gameLoop;
        public PlayableArea PlayableArea => _playableArea;
        public DifficultyLevel DifficultyLevel => _difficultyLevel;
        public CannonDefinition CannonDefinition => _cannonDef;
        public GameStateSystem GameStateSystem { get; private set; }
        public StatModel StatModel => _statModel;
        public PauseSystem PauseSystem { get; private set; }

        public static GameSession Instance;

        private void Awake()
        {
            Instance ??= this;
            InitSystems();
            GameStateSystem.ChangeGameState(this, GameState.Playing);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _sceneService.LoadScene(_mainMenuScene);
            }
        }

        private void InitSystems()
        {
            PauseSystem = new PauseSystem();
            GameStateSystem = new GameStateSystem();
            _statModel = new StatModel(_playerLevels);
        }
    }
}