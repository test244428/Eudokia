﻿using System;
using UnityEngine;

namespace Eudokia.Model.Data.Properties
{
    [Serializable]
    public class ObservableProperty<TPropertyType>
    {
        [SerializeField] protected TPropertyType _value;

        public delegate void OnPropertyChanged(TPropertyType newValue, TPropertyType oldValue);
        public event OnPropertyChanged OnChanged;

        public virtual TPropertyType Value
        {
            get => _value;
            set
            {
                bool isSame = _value?.Equals(value) ?? false;
                if (isSame) return;
                TPropertyType oldValue = _value;
                _value = value;
                InvokeOnChangedEvent(_value, oldValue);
            }
        }

        protected void InvokeOnChangedEvent(TPropertyType newValue, TPropertyType oldValue) =>
            OnChanged?.Invoke(newValue, oldValue);
    }
}