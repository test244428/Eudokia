﻿using System;

namespace Eudokia.Model.Data.Properties
{
    [Serializable]
    public class IntProperty : ObservableProperty<int> { }
}