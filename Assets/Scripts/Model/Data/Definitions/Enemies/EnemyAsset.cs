﻿using UnityEngine;

namespace Eudokia.Model.Data.Definitions.Enemies
{
    [CreateAssetMenu(fileName = "EnemyConstructor", menuName = "Definitions/EnemyConstructor")]
    public class EnemyAsset : ScriptableObject
    {
        [SerializeField] private int _baseHp;
        [SerializeField] private GameObject _modelPrefab;
        
        public int BaseHp => _baseHp;
        public GameObject ModelPrefab => _modelPrefab;
    }
}