﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Eudokia.Model.Data.Definitions.Player
{
    [Serializable]
    public class LevelData
    {
        [SerializeField] private List<LevelProgress> _progresses;

        public int GetLevel(StatId id)
        {
            foreach (var progress in _progresses)
            {
                if (progress.Id == id) return progress.Level;
            }

            return 0;
        }

        public void LevelUp(StatId id)
        {
            var progress = _progresses.FirstOrDefault(x => x.Id == id);
            if (progress == null) _progresses.Add(new LevelProgress(id, 1));
            else progress.Level++;
        }
    }

    [Serializable]
    public class LevelProgress
    {
        [SerializeField] private StatId _id;
        [SerializeField] private int _level;
        public StatId Id => _id;
        public int Level
        {
            get => _level;
            set => _level = value;
        }

        public LevelProgress(StatId id, int level)
        {
            _id = id;
            _level = level;
        }
    }
}