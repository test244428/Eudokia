﻿using UnityEngine;

namespace Eudokia.Model.Data.Definitions.Player
{
    [CreateAssetMenu(fileName = "CannonDef", menuName = "Definitions/Cannon")]
    public class CannonDefinition : ScriptableObject
    {
        [SerializeField] private StatDef[] _stats;
        
        public StatDef GetStat(StatId id)
        {
            foreach (var stat in _stats)
            {
                if (stat.ID == id) return stat;
            }

            return default;
        }
    }
}