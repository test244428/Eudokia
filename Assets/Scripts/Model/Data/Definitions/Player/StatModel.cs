﻿namespace Eudokia.Model.Data.Definitions.Player
{
    public class StatModel
    {
        private readonly LevelData _levelData;

        public StatModel(LevelData data)
        {
            _levelData = data;
        }
        
        public void LevelUp(StatId id)
        {
            var statDef = GameSession.Instance.CannonDefinition.GetStat(id);
            var nextLevel = GetCurrentLevel(id) + 1;
            if (statDef.Levels.Length <= nextLevel) return;
            _levelData.LevelUp(id);
        }

        public float GetValue(StatId id, int level = -1)
        {
            if (level == -1) level = GetCurrentLevel(id);
            return GetStatLevelDef(id, level).Value;
        }

        private StatLevelDef GetStatLevelDef(StatId id, int level)
        {
            var cannonDef = GameSession.Instance.CannonDefinition;
            var statDef = cannonDef.GetStat(id);
            return statDef.Levels.Length > level ? statDef.Levels[level] : default;
        }

        public bool IsMaxLevel(StatId id) =>
            GetCurrentLevel(id) + 1 == GameSession.Instance.CannonDefinition.GetStat(id).Levels.Length;

        public int GetCurrentLevel(StatId id) => _levelData.GetLevel(id);
    }
}