﻿using UnityEngine;

namespace Eudokia.Model.Data.Definitions.Difficulty
{
    [CreateAssetMenu(fileName = "Difficult", menuName = "Definitions/Difficult")]

    public class DifficultyLevel : ScriptableObject
    {
        [SerializeField] private DifficultyDef[] _difficultyDef;

        public DifficultyDef Get(int level)
        {
            var clampedLevel = Mathf.Clamp(level, 0, _difficultyDef.Length - 1);
            return _difficultyDef[clampedLevel];
        }
    }
}