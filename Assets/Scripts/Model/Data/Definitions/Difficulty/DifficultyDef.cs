﻿using System;
using UnityEngine;

namespace Eudokia.Model.Data.Definitions.Difficulty
{
    [Serializable]
    public struct DifficultyDef
    {
        [SerializeField] private float _timeBetweenSpawn;
        [SerializeField] private float _hpModifier;
        [SerializeField] private float _speedModifier;

        public float TimeBetweenSpawn => _timeBetweenSpawn;
        public float HpModifier => _hpModifier;
        public float SpeedModifier => _speedModifier;
    }
}