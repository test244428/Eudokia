﻿using Eudokia.Model;
using Eudokia.Model.Data.Definitions.Difficulty;
using Eudokia.UI.Level;
using UnityEngine;

namespace Eudokia.GameLoop
{
    public class GameLoopSystem : MonoBehaviour
    {
        [Header("Game settings")] 
        [SerializeField, Min(10)] private int _enemiesToLose;
        [SerializeField,Min(1)] private int _enemiesToLevelUp;
        [Header("UI"), Space]
        [SerializeField] private LevelUpUI _levelUpUI;

        private GameSession _session;
        private int _currentEnemiesAmount;
        private int _enemiesKilled;
        private int _currentLevel;
        
        public DifficultyDef CurrentDifficulty => _session.DifficultyLevel.Get(_currentLevel);

        private void Start()
        {
            _session = GameSession.Instance;
        }

        public void RegisterUnit()
        {
            _currentEnemiesAmount++;
            if (_currentEnemiesAmount >= _enemiesToLose)
            {
                _session.GameStateSystem.ChangeGameState(this, GameState.Lose);
            }
        }

        public void UnregisterUnit()
        {
            _currentEnemiesAmount--;
            _enemiesKilled++;
            if (_enemiesKilled % _enemiesToLevelUp == 0) LevelUp();
        }

        private void LevelUp()
        {
            _currentLevel++;
            _levelUpUI.LevelUp();
        }
    }
}