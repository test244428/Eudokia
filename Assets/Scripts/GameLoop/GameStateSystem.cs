﻿using Eudokia.Model;
using UnityEngine;

namespace Eudokia.GameLoop
{
    public class GameStateSystem
    {
        private GameState _currentGameState;

        public GameState CurrentGameState => _currentGameState;

        public delegate void GameStateChange(GameState newState, GameState oldState);
        public event GameStateChange OnGameStateChanged;
        
        public void ChangeGameState(object sender ,GameState newState)
        {
            //Debug.Log($"{sender} set state {newState.ToString()}");
            if (_currentGameState == newState) return;
            GameState oldState = _currentGameState;
            _currentGameState = newState;
            GameSession.Instance.PauseSystem.SetPaused(newState == GameState.Pause);
            OnGameStateChanged?.Invoke(newState, oldState);
        }
    }
    
    public enum GameState
    {
        Playing,
        Pause,
        Lose,
        Freeze
    }
}