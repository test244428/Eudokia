﻿using Eudokia.GameLoop;
using Eudokia.Utils;
using UnityEngine;

namespace Eudokia.Components.Powerups
{
    public class FreezeTimeBuff : Buff
    {
        [Header("Settings")]
        [SerializeField] private Timer _freezeTime;

        private GameState _previousState;
        private float _timeLeftForFreezeAfterPause;

        protected override void Update()
        {
            base.Update();
            if (_freezeTime.IsReady && Session.GameStateSystem.CurrentGameState == GameState.Freeze)
            {
                Session.GameStateSystem.ChangeGameState(this, GameState.Playing);
                Pool.Instance.Destroy(gameObject);
            }
        }

        protected override void ApplyBuff()
        {
            if (Session.GameStateSystem.CurrentGameState == GameState.Lose) return;

            Session.GameStateSystem.ChangeGameState(this, GameState.Freeze);
            _freezeTime.Reset();
        }

    }
}