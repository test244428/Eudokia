﻿using Eudokia.Utils;
using UnityEngine;

namespace Eudokia.Components.Powerups
{
    public class BuffSpawner : MonoBehaviour
    {
        [SerializeField] private Transform[] _spawnTransforms;
        [SerializeField] private Timer _spawnPeriod;
        [SerializeField] private Buff[] _buffPrefabs;
        
        private void Start()
        {
            _spawnPeriod.Reset();
        }

        private void Update()
        {
            if (_spawnPeriod.IsReady)
            {
                _spawnPeriod.Reset();
                SpawnBuff();
            }
        }

        private void SpawnBuff()
        {
            var randomBuff = _buffPrefabs[Random.Range(0, _buffPrefabs.Length)];
            var randomPosition = _spawnTransforms[Random.Range(0, _spawnTransforms.Length)].position;
            Pool.Instance.Get(randomBuff.gameObject, randomPosition, randomBuff.transform.rotation);
        }
    }
}