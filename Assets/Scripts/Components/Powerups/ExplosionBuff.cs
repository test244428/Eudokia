﻿using Eudokia.Utils;
using UnityEngine;

namespace Eudokia.Components.Powerups
{
    public class ExplosionBuff : Buff
    {
        [SerializeField] private Explosion _explosionPrefab;
        
        protected override void ApplyBuff()
        {
            Pool.Instance.Get(_explosionPrefab.gameObject, Vector3.zero, Quaternion.identity);

            Pool.Instance.Destroy(gameObject);
        }
    }
}