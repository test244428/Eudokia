﻿using Eudokia.Interfaces;
using Eudokia.Model;
using Eudokia.Utils;
using Eudokia.Utils.Pause;
using UnityEngine;

namespace Eudokia.Components.Powerups
{
    [RequireComponent(typeof(PoolItem))]
    public abstract class Buff : MonoBehaviour, IDamageable, IPauseable
    {
        [SerializeField] private float _rotateSpeed;
        
        [Space, Header("Components")]
        [SerializeField] private Collider _collider;
        [SerializeField] private MeshRenderer[] _meshes;

        protected bool IsActive = true;
        
        protected GameSession Session;

        protected virtual void Start()
        {
            Session = GameSession.Instance;
            Session.PauseSystem.Register(this);
        }

        public virtual void Restart()
        {
            _collider.enabled = true;
            foreach (var mesh in _meshes) mesh.enabled = true;
            IsActive = true;
        }

        protected virtual void Update()
        {
            if (IsActive) transform.Rotate(Vector3.forward, _rotateSpeed * Time.deltaTime);
        }

        public void ApplyDamage(int _)
        {
            if (!IsActive) return;
            
            _collider.enabled = false;
            foreach (var mesh in _meshes) mesh.enabled = false;
            IsActive = false;
            ApplyBuff();
        }

        protected abstract void ApplyBuff();

        public void SetPaused(bool isPaused)
        {
            IsActive = !isPaused;
        }

        private void OnDestroy()
        {
            Session.PauseSystem.UnRegister(this);
        }
    }
}