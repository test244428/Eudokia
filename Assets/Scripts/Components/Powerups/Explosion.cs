﻿using Eudokia.Interfaces;
using Eudokia.Utils;
using UnityEngine;

namespace Eudokia.Components.Powerups
{
    [RequireComponent(typeof(PoolItem))]
    public class Explosion : MonoBehaviour
    {
        [SerializeField] private int _explosionDamage = 1000;
        [SerializeField] private float _maxSize = 50f;
        [SerializeField] private float _explosionTime = .5f;
        [SerializeField] private float _rotateSpeed = 50f;
        [SerializeField] private string _enemyTag;

        private float _timePassed;

        private void Start()
        {
            _timePassed = 0;
        }

        public void Restart()
        {
            _timePassed = 0;
            transform.localScale = Vector3.one;
        }

        private void Update()
        {
            float interpolationValue = _timePassed / _explosionTime;
            float scale = Mathf.Lerp(1, _maxSize, interpolationValue);
            transform.localScale = new Vector3(scale, scale, scale);
            transform.Rotate(Vector3.one, _rotateSpeed * Time.deltaTime);
            _timePassed += Time.deltaTime;
            if (_timePassed >= _explosionTime) Pool.Instance.Destroy(gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag(_enemyTag))
            {
                return;
            }
            
            if (other.gameObject.TryGetComponent(out IDamageable damageable))
            {
                damageable.ApplyDamage(_explosionDamage);
            }
        }
    }
}