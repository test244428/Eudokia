﻿using UnityEngine;

namespace Eudokia.Components.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AmbientChanger : MonoBehaviour
    {
        [SerializeField] private AudioData[] _clipsData;
        private AudioSource _source;

        private void Awake()
        {
            _source = GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (_source.isPlaying) return;

            _source.clip = _clipsData[Random.Range(0, _clipsData.Length)].Clip;
            _source.Play();
        }
    }
}