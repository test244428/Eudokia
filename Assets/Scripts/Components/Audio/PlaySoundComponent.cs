﻿using UnityEngine;

namespace Eudokia.Components.Audio
{
    public class PlaySoundComponent : MonoBehaviour
    {
        [SerializeField] private AudioSource _source;
        [SerializeField] private AudioData _sound;

        public void Play()
        {
            _source.PlayOneShot(_sound.Clip);
        }
    }
}