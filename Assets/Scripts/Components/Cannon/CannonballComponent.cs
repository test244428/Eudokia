﻿using Eudokia.Interfaces;
using Eudokia.Model;
using Eudokia.Utils;
using Eudokia.Utils.Pause;
using UnityEngine;

namespace Eudokia.Components.Cannon
{
    public class CannonballComponent : MonoBehaviour, IPauseable
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Timer _destroyLifetime;

        private int _damage;
        private bool _isProjectileHit;
        private bool _isDefuseInProgress;

        public Rigidbody Rigidbody => _rigidbody;

        public int Damage
        {
            set => _damage = value <= 0 ? 0 : value;
        }

        private void Start()
        {
            GameSession.Instance.PauseSystem.Register(this);
        }

        private void Update()
        {
            if (!_isDefuseInProgress) return;

            if (_destroyLifetime.IsReady)
            {
                _isDefuseInProgress = false;
                Pool.Instance.Destroy(gameObject);
            }
        }

        public void OnPoolItemRestart()
        {
            _isProjectileHit = false;
            _isDefuseInProgress = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_isProjectileHit) return;

            if (other.gameObject.TryGetComponent(out IDamageable damageable))
            {
                damageable.ApplyDamage(_damage);
                _isProjectileHit = true;
            }
            
            DefuseCannonball();
        }

        private void DefuseCannonball()
        {
            if (_isDefuseInProgress) return;
            _isDefuseInProgress = true;
            _destroyLifetime.Reset();
        }

        public void SetPaused(bool isPaused)
        {
            //?
        }

        private void OnDestroy()
        {
            GameSession.Instance.PauseSystem.UnRegister(this);
        }
    }
}