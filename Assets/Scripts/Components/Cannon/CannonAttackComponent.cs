﻿using Eudokia.Components.Audio;
using Eudokia.GameLoop;
using Eudokia.Model;
using Eudokia.Model.Data.Definitions.Player;
using Eudokia.Utils;
using UnityEngine;

namespace Eudokia.Components.Cannon
{
    [RequireComponent(typeof(PlaySoundComponent))]
    public class CannonAttackComponent : MonoBehaviour
    {
        [Header("Systems")]
        [SerializeField] private Transform _cannonballBarrelTransform;
        [SerializeField] private LayerMask _aimPlaneLayerMask;
        [SerializeField] private GameObject _cannonballComponentPrefab;
        [Space, Header("Cannon settings")]
        [SerializeField] private ForceMode _forceMode;
        [SerializeField, Min(0f)] private float _force = 10f;

        private Vector3 _cannonAim;
        private PlaySoundComponent _soundComponent;
        private GameSession _session;
        private Timer _shootCooldown = new();

        private void Awake()
        {
            _soundComponent = GetComponent<PlaySoundComponent>();
        }

        private void Start()
        {
            _session = GameSession.Instance;
            _shootCooldown.Value = _session.StatModel.GetValue(StatId.ShootSpeed);
        }

        private void Update()
        {
            _cannonAim = GetAimPosition();
            if (Input.GetMouseButtonDown(0)) PerformAttack();
        }

        private void PerformAttack()
        {
            if (_session.GameStateSystem.CurrentGameState is GameState.Pause or GameState.Lose) return;
            if (!_shootCooldown.IsReady) return;
            
            var cannonball = GetCannonball();
            cannonball.Damage = (int) _session.StatModel.GetValue(StatId.Damage);
            var fixedAim = _cannonAim - _cannonballBarrelTransform.position;
            fixedAim.Normalize();
            cannonball.Rigidbody.AddForce(fixedAim * _force, _forceMode);
            _shootCooldown.Value = _session.StatModel.GetValue(StatId.ShootSpeed);
            _shootCooldown.Reset();
            DoEffects();
        }

        private void DoEffects()
        {
            _soundComponent.Play();
        }

        private CannonballComponent GetCannonball()
        {
            var cannonballGO = Pool.Instance.Get(_cannonballComponentPrefab, _cannonballBarrelTransform.position, _cannonballBarrelTransform.rotation);
            var cannonball = cannonballGO.GetComponent<CannonballComponent>();
            cannonball.Rigidbody.velocity = Vector3.zero;
            return cannonball;
        }

        private Vector3 GetAimPosition()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out RaycastHit hit, float.MaxValue, _aimPlaneLayerMask);
            return hit.point;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(_cannonballBarrelTransform.position, _cannonAim);
        }
#endif
    }
}