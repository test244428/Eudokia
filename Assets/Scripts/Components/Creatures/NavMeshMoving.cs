﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Eudokia.Components.Creatures
{
    public class NavMeshMoving
    {
        private NavMeshAgent _agent;

        public event Action OnNavMeshReachDestination;

        public NavMeshMoving(NavMeshAgent agent)
        {
            _agent = agent;
        }

        public void Move(Vector3 destination)
        {
            _agent.SetDestination(destination);
        }

        public void CheckAgentReachDestination()
        { 
            if (_agent.pathPending) return;
            if (_agent.remainingDistance > _agent.stoppingDistance) return;
            if (_agent.hasPath && _agent.velocity.sqrMagnitude != 0f) return;
            OnNavMeshReachDestination?.Invoke();
        }
    }
}