﻿using System.Collections;
using Eudokia.Components.Audio;
using Eudokia.Interfaces;
using Eudokia.Model;
using Eudokia.Model.Data.Properties;
using Eudokia.Utils;
using Eudokia.Utils.Pause;
using UnityEngine;
using UnityEngine.AI;

namespace Eudokia.Components.Creatures
{
    [RequireComponent(typeof(PlaySoundComponent))]
    public class Enemy : MonoBehaviour, IDamageable, IPauseable
    {
        [SerializeField] private NavMeshAgent _agent;
        [SerializeField] private Animator _animator;
        [SerializeField] private float _corpseDisappearTime;

        private float _baseSpeed;
        private float _baseAnimatorSpeed;
        private IntProperty _hp = new();
        private bool _isAlive;
        private bool _isEnemyInited;
        private NavMeshMoving _pathfinding;
        private GameSession _session;
        private PlaySoundComponent _soundComponent;

        private static readonly int Hit = Animator.StringToHash("Hit");

        private void Awake()
        {
            _soundComponent = GetComponent<PlaySoundComponent>();
        }

        private void Start()
        {
            _hp.OnChanged += OnHpChanged;
            _pathfinding.OnNavMeshReachDestination += MoveToNewPosition;
            _session.PauseSystem.Register(this);
        }

        private void Update()
        {
            if (_isAlive) _pathfinding.CheckAgentReachDestination();
        }

        public void AnimateEnemy(int baseHp, float hpModifier, float speedModifier)
        {
            if (!_isEnemyInited) InitEnemy();
            _agent.isStopped = false;
            _isAlive = true;
            _animator.enabled = true;
            _hp.Value = (int) (baseHp * hpModifier);
            _agent.speed = _baseSpeed * speedModifier;
        }

        private void InitEnemy()
        {
            _isEnemyInited = true;
            _session = GameSession.Instance;
            _pathfinding = new NavMeshMoving(_agent);
            _baseSpeed = _agent.speed;
            _baseAnimatorSpeed = _animator.speed;
        }

        private void OnHpChanged(int newValue, int oldValue)
        {
            if (newValue >= oldValue) return;
            
            if (newValue <= 0) PerformDeath();
            else PerformHit();
        }

        private void MoveToNewPosition()
        {
            if (_isAlive) _pathfinding.Move(_session.PlayableArea.GetRandomPlayablePosition());
        }

        private void PerformHit()
        {
            _animator.SetTrigger(Hit);
            _soundComponent.Play();
        }

        private void PerformDeath()
        {
            _isAlive = false;
            _agent.isStopped = true;
            _animator.enabled = false;
            _session.GameLoop.UnregisterUnit();
            StartCoroutine(DisappearCorpse());
        }

        private IEnumerator DisappearCorpse()
        {
            yield return new WaitForSeconds(_corpseDisappearTime);
            Pool.Instance.Destroy(gameObject);
        }

        public void ApplyDamage(int damage)
        {
            if (!_isAlive) return;
            _hp.Value -= damage;
        }

        private void OnDestroy()
        {
            _hp.OnChanged -= OnHpChanged;
            _pathfinding.OnNavMeshReachDestination -= MoveToNewPosition;
            _session.PauseSystem.UnRegister(this);
        }

        public void SetPaused(bool isPaused)
        {
            if (!_agent.isActiveAndEnabled) return;
            _agent.isStopped = isPaused;
            _animator.speed = isPaused ? 0 : _baseAnimatorSpeed;
        }
    }
}