using System.Collections.Generic;
using Eudokia.GameLoop;
using Eudokia.Model;
using Eudokia.Model.Data.Definitions.Difficulty;
using Eudokia.Model.Data.Definitions.Enemies;
using Eudokia.Utils;
using Eudokia.Utils.Pause;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Eudokia.Components.Creatures
{
    public class EnemySpawner : MonoBehaviour, IPauseable
    {
        [SerializeField] private List<EnemyAsset> _enemiesList;

        private GameSession _session;
        private DifficultyDef CurrentDifficulty => _session.GameLoop.CurrentDifficulty;

        private bool _isActive;
        private Timer _spawnTimer = new();

        private void Start()
        {
            _session = GameSession.Instance;
            _session.PauseSystem.Register(this);
            _session.GameStateSystem.OnGameStateChanged += CheckNewState;
            _spawnTimer.Value = CurrentDifficulty.TimeBetweenSpawn;
            _isActive = true;
        }

        private void CheckNewState(GameState newstate, GameState oldstate)
        {
            if (newstate == GameState.Freeze)
            {
                _isActive = false;
                _spawnTimer.SetPaused(true);
            }

            if (oldstate == GameState.Freeze)
            {
                _isActive = true;
                _spawnTimer.SetPaused(false);
            }
        }

        private void Update()
        {
            if (_isActive && _spawnTimer.IsReady)
            {
                Spawn();
                _spawnTimer.Value = CurrentDifficulty.TimeBetweenSpawn;
                _spawnTimer.Reset();
            }
        }

        private void Spawn()
        {
            Vector3 randomPosition = _session.PlayableArea.GetRandomPlayablePosition();
            int randomEnemyIndex = Random.Range(0, _enemiesList.Count);
            var enemyAsset = _enemiesList[randomEnemyIndex];

            Enemy enemy = Pool.Instance.Get(enemyAsset.ModelPrefab, randomPosition, Quaternion.identity)
                .GetComponent<Enemy>();

            enemy.AnimateEnemy(enemyAsset.BaseHp, CurrentDifficulty.HpModifier, CurrentDifficulty.SpeedModifier);
            _session.GameLoop.RegisterUnit();
        }

        public void SetPaused(bool isPaused)
        {
            _isActive = !isPaused;
        }

        private void OnDestroy()
        {
            _session.PauseSystem.UnRegister(this);
        }
    }
}