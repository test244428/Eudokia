﻿using UnityEngine;

namespace Eudokia.Utils
{
    public class PlayableArea : MonoBehaviour
    {
        private float _halfSpawnSizeX;
        private float _halfSpawnSizeZ;
        private Vector3 _playableLossyScale;

        private void Awake()
        {
            _playableLossyScale = transform.lossyScale;
            _halfSpawnSizeX = _playableLossyScale.x / 2;
            _halfSpawnSizeZ = _playableLossyScale.z / 2;
        }
        
        public Vector3 GetRandomPlayablePosition()
        {
            float randomPositionX = Random.Range(0, _playableLossyScale.x) - _halfSpawnSizeX;
            float randomPositionZ = Random.Range(0, _playableLossyScale.z) - _halfSpawnSizeZ;
            Vector3 randomPosition = new Vector3(randomPositionX, 0, randomPositionZ);
            return randomPosition;
        }
    }
}