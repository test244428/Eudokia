﻿using System;
using Eudokia.Utils.Pause;
using UnityEngine;

namespace Eudokia.Utils
{
    [Serializable]
    public class Timer : IPauseable
    {
        [SerializeField] private float _value;
        private float _timeUp;
        private float _remainingTimeAfterPause;

        public float RemainingTime => Mathf.Max(_timeUp - Time.time, 0f);

        public bool IsReady => _timeUp <= Time.time;

        public float Value
        {
            get => _value;
            set => _value = value;
        }

        public void Reset()
        {
            _timeUp = Time.time + _value;
        }

        public void EarlyComplete()
        {
            _timeUp -= _value;
        }

        public void SetPaused(bool isPaused)
        {
            if (isPaused)
            {
                _remainingTimeAfterPause = RemainingTime;
            }
            else
            {
                _timeUp = Time.time + _remainingTimeAfterPause;
            }
        }
    }
}