﻿using System.Collections.Generic;

namespace Eudokia.Utils.Pause
{
    public class PauseSystem : IPauseable
    {
        private readonly List<IPauseable> _handlers = new();

        private bool _isPaused;

        public void Register(IPauseable handler)
        {
            _handlers.Add(handler);
        }

        public void UnRegister(IPauseable handler)
        {
            _handlers.Remove(handler);
        }

        public void SetPaused(bool isPaused)
        {
            if (_isPaused == isPaused) return;
            
            _isPaused = isPaused;
            
            foreach (var handler in _handlers)
            {
                handler.SetPaused(isPaused);
            }
        }
    }
}