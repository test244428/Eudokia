﻿namespace Eudokia.Utils.Pause
{
    public interface IPauseable
    {
        public void SetPaused(bool isPaused);
    }
}