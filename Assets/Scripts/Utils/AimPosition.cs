﻿using UnityEngine;

namespace Eudokia.Utils
{
    public class AimPosition : MonoBehaviour
    {
        [SerializeField] private LayerMask _aimLayerMask;

        private void Update()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit, float.MaxValue, _aimLayerMask))
            {
                transform.position = hit.point;
            }
        }
    }
}