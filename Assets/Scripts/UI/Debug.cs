﻿using Eudokia.Model;
using TMPro;
using UnityEngine;

namespace Eudokia.UI
{
    public class Debug : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _currentState;

        private void Start()
        {
            GameSession.Instance.GameStateSystem.OnGameStateChanged +=
                (state, oldState) => _currentState.text = state.ToString();
            _currentState.text = GameSession.Instance.GameStateSystem.CurrentGameState.ToString();
        }
    }
}