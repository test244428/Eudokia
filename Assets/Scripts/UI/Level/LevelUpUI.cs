﻿using Eudokia.GameLoop;
using Eudokia.Model;
using Eudokia.Model.Data.Definitions.Player;
using UnityEngine;

namespace Eudokia.UI.Level
{
    public class LevelUpUI : MonoBehaviour
    {
        [SerializeField] private StatWidget _damageWidget;
        [SerializeField] private StatWidget _shootSpeedWidget;
        private GameSession _session;
        private GameState _previousGameState;
        
        private void Start()
        {
            _session = GameSession.Instance;
            _session.GameStateSystem.OnGameStateChanged += (_, oldState) => _previousGameState = oldState;
            _damageWidget.OnLevelUpComplete += LevelUpComplete;
            _shootSpeedWidget.OnLevelUpComplete += LevelUpComplete;
            gameObject.SetActive(false);
        }

        public void LevelUp()
        {
            if (_session.StatModel.IsMaxLevel(StatId.Damage) && 
                _session.StatModel.IsMaxLevel(StatId.ShootSpeed)) return;
            
            _damageWidget.UpdateWidget();
            _shootSpeedWidget.UpdateWidget();

            gameObject.SetActive(true);
            _session.GameStateSystem.ChangeGameState(this, GameState.Pause);
        }

        private void LevelUpComplete()
        {
            gameObject.SetActive(false);
            _session.GameStateSystem.ChangeGameState(this, _previousGameState);
        }

        private void OnDestroy()
        {
            _damageWidget.OnLevelUpComplete -= LevelUpComplete;
            _shootSpeedWidget.OnLevelUpComplete -= LevelUpComplete;
        }
    }
}