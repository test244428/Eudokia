﻿using System;
using Eudokia.Model;
using Eudokia.Model.Data.Definitions.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Eudokia.UI.Level
{
    public class StatWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _statNameText;
        [SerializeField] private TextMeshProUGUI _levelText;
        [SerializeField] private Image _statIcon;
        [SerializeField] private StatId _statId;
        
        public event Action OnLevelUpComplete;
        public void UpdateWidget()
        {
            if (GameSession.Instance.StatModel.IsMaxLevel(_statId)) 
                gameObject.SetActive(false);
            
            var statDef = GameSession.Instance.CannonDefinition.GetStat(_statId);
            _statNameText.text = statDef.Name;
            var currentLevel = GameSession.Instance.StatModel.GetCurrentLevel(_statId) + 1;
            _levelText.text = $"{currentLevel} / {statDef.Levels.Length}";
            _statIcon.sprite = statDef.Icon;
        }

        public void LevelUp()
        {
            GameSession.Instance.StatModel.LevelUp(_statId);
            OnLevelUpComplete?.Invoke();
        }
    }
}